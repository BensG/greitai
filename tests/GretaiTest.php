<?php

use App\Airlines\AirlineFactory;

class GreitaiTest extends TestCase
{
    /**
     * @dataProvider getRyanairRoutesArray
     */
    public function testRyanairProcessRoutes($routes, $expected)
    {
        $ryanair = AirlineFactory::build('Ryanair');
    
        $reflection = new \ReflectionClass($ryanair);

        $method = $reflection->getMethod('processRoutes');
        $method->setAccessible(true);
        $directRoutes = $method->invokeArgs($ryanair, [$this->arrayToObject($routes), 'direct']);

        $this->assertEquals($directRoutes, $expected);
    }

    /**
     * @dataProvider getRyanairProcessFlightArray
     */
    public function testRyanairProcessFlight($identifier, $response, $expected)
    {
        $ryanair = AirlineFactory::build('Ryanair');
    
        $reflection = new \ReflectionClass($ryanair);

        $method = $reflection->getMethod('processFlight');
        $method->setAccessible(true);
        $flight = $method->invokeArgs($ryanair, [$response, $identifier]);

        $this->assertEquals($flight, $expected);
    }

    private function arrayToObject(array $routes): array
    {
        $returnRoutes = [];

        foreach ($routes as $route) {
            $returnRoutes[] = (object) $route;
        }

        return $returnRoutes;
    }

    public static function getRyanairProcessFlightArray()
    {
        // Objektas nerandamo skrydžio atveju
        $flightNotFound = new stdClass;
        $flightNotFound->currency = 'EUR';

        $flightNotFoundTrip = new stdClass;
        $flightNotFoundTrip->origin = 'BGY';
        $flightNotFoundTrip->destination = 'FAO';

        $flightNotFoundDate = new stdClass;
        $flightNotFoundDate->flights = [];

        $flightNotFoundTrip->dates[] = $flightNotFoundDate;

        $flightNotFound->trips = [$flightNotFoundTrip];
    
        // Objektas išparduotų bilietų atveju
        $flightSoldOut = new stdClass;
        $flightSoldOut->currency = 'EUR';

        $flightSoldOutTrip = new stdClass;
        $flightSoldOutTrip->origin = 'BGY';
        $flightSoldOutTrip->destination = 'FAO';

        $flightSoldOutFlights = new stdClass;
        $flightSoldOutFlights->faresLeft = 0;

        $flightSoldOutDate = new stdClass;
        $flightSoldOutDate->flights[] = $flightSoldOutFlights;

        $flightSoldOutTrip->dates[] = $flightSoldOutDate;

        $flightSoldOut->trips = [$flightSoldOutTrip];

        // Objektas normaliu atveju
        $flight = new stdClass;
        $flight->currency = 'EUR';

        $flightTrip = new stdClass;
        $flightTrip->origin = 'BGY';
        $flightTrip->destination = 'FAO';

        $flightADTFare = new stdClass;
        $flightADTFare->type = 'ADT';
        $flightADTFare->amount = 372;

        $flightCHDFare = new stdClass;
        $flightCHDFare->type = 'CHD';
        $flightCHDFare->amount = 20;

        $flightRegularFare = new stdClass;
        $flightRegularFare->fares = [$flightADTFare,  $flightCHDFare];

        $flightSegmentOne = new stdClass;
        $flightSegmentOne->origin = 'BGY';
        $flightSegmentOne->destination = 'OPO';
        $flightSegmentOne->flightNumber = 'FR 4012';
        $flightSegmentOne->time = ['2018-03-18T17:15:00.000', '2018-03-18T18:50:00.000'];
        $flightSegmentOne->duration = '02:35';

        $flightSegmentTwo = new stdClass;
        $flightSegmentTwo->origin = 'OPO';
        $flightSegmentTwo->destination = 'FAO';
        $flightSegmentTwo->flightNumber = 'FR 5486';
        $flightSegmentTwo->time = ['2018-03-18T22:05:00.000', '2018-03-18T23:15:00.000'];
        $flightSegmentTwo->duration = '01:10';

        $flightFlights = new stdClass;
        $flightFlights->faresLeft = 3;
        $flightFlights->regularFare = $flightRegularFare;
        $flightFlights->segments = [$flightSegmentOne,  $flightSegmentTwo];
        $flightFlights->time = ['2018-03-18T17:15:00.000',  '2018-03-18T23:15:00.000'];
        $flightFlights->duration = '07:00';

        $flightDate = new stdClass;
        $flightDate->flights[] = $flightFlights;

        $flightTrip->dates[] = $flightDate;

        $flight->trips = [$flightTrip];

        return [
            [
                'randomIdentifier1',
                [
                    'success' => 1,
                    'response' => $flightNotFound,
                    'errors' => ''
                ],
                [
                    'randomIdentifier1' => null,
                    'error' => [
                        'identifier' => 'randomIdentifier1',
                        'code' => 12
                    ]
                ]
            ],
            [
                'randomIdentifier2',
                [
                    'success' => 1,
                    'response' => $flightSoldOut,
                    'errors' => ''
                ],
                [
                    'randomIdentifier2' => null,
                    'error' => [
                        'identifier' => 'randomIdentifier2',
                        'code' => 11
                    ]
                ]
            ],
            [
                'randomIdentifier3',
                [
                    'success' => 1,
                    'response' => $flight,
                    'errors' => ''
                ],
                [
                    'randomIdentifier3' => [
                        'origin' => 'BGY',
                        'destination' => 'FAO',
                        'departureTime' => '2018-03-18 17:15:00',
                        'arrivalTime' => '2018-03-18 23:15:00',
                        'duration' => 420,
                        'price' => [
                            [
                                'paxType' => 'ADT',
                                'amount' => 379.44,
                                'currency' => 'EUR'
                            ],
                            [
                                'paxType' => 'CHD',
                                'amount' => 20.4,
                                'currency' => 'EUR'
                            ],
                            [
                                'paxType' => 'INF',
                                'amount' => 25,
                                'currency' => 'EUR'
                            ]
                        ],
                        'segments' => [
                            [
                                'origin' => 'BGY',
                                'destination' => 'OPO',
                                'departureTime' => '2018-03-18 17:15:00',
                                'arrivalTime' => '2018-03-18 18:50:00',
                                'flightNumber' => 'FR4012',
                                'duration' => 155
                            ],
                            [
                                'origin' => 'OPO',
                                'destination' => 'FAO',
                                'departureTime' => '2018-03-18 22:05:00',
                                'arrivalTime' => '2018-03-18 23:15:00',
                                'flightNumber' => 'FR5486',
                                'duration' => 70
                            ]
                        ]
                    ]
                ]
            ],
            [
                'randomIdentifier4',
                [
                    'success' => 0,
                    'response' => 'error',
                    'errors' => [
                        'statusCode' => 400,
                        'reasonPhrase' => 'Bad Request'
                    ]
                ],
                [
                    'randomIdentifier4' => null,
                    'error' => [
                        'identifier' => 'randomIdentifier4',
                        'code' => 20
                    ]
                ]
            ],
        ];
    }

    public static function getRyanairRoutesArray(): array
    {
        return [
            [
                [
                    [
                        'airportFrom' => 'STN',
                        'airportTo' => 'HAM',
                        'connectingAirport' => '',
                        'newRoute' => '',
                        'seasonalRoute' => '', 
                        'operator' => 'RYANAIR',
                        'group' => 'CITY',
                    ],
                    [
                        'airportFrom' => 'SUF',
                        'airportTo' => 'PSA',
                        'connectingAirport' => '',
                        'newRoute' => '',
                        'seasonalRoute' => '',
                        'operator' => 'RYANAIR',
                        'group' => 'DOMESTIC'
                    ],
                    [
                        'airportFrom' => 'MIA',
                        'airportTo' => 'LCG',
                        'connectingAirport' => 'MAD',
                        'newRoute' => '',
                        'seasonalRoute' => '',
                        'operator' => 'AIR_EUROPA',
                        'group' => 'GENERIC'
                    ],
                    [
                        'airportFrom' => 'BCN',
                        'airportTo' => 'MLA',
                        'connectingAirport' => 'FCO',
                        'newRoute' => '',
                        'seasonalRoute' => '',
                        'operator' => 'RYANAIR',
                        'group' => 'GENERIC'
                    ],
                    [
                        'airportFrom' => 'NAP',
                        'airportTo' => 'EIN',
                        'connectingAirport' => null,
                        'newRoute' => '',
                        'seasonalRoute' => '',
                        'operator' => 'RYANAIR',
                        'group' => 'GENERIC'
                    ]
                ],
                [
                    [
                        'origin' => 'STN',
                        'destination' => 'HAM'
                    ],
                    [
                        'origin' => 'SUF',
                        'destination' => 'PSA'
                    ],
                    [
                        'origin' => 'NAP',
                        'destination' => 'EIN'
                    ]
                ]
            ]
        ];
    }
}
