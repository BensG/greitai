Projekto paleidimas:

Darbinėje aplinkoje (aš naudojau https://laravel.com/docs/5.6/homestead) nusiklonuoti projektą ir paleisti composer install komandą

git clone https://BensG@bitbucket.org/BensG/greitai.git

composer install

**API**

Tiesioginiai skrydžiai:


```
#!php

curl -X GET -H "Content-Type: application/json" http://[localhost]/api/ryanair/routes
```


Skrydžiai pagal naudotojo nurodytas sąlygas:


```
#!Curl

curl -X GET -H "Content-Type: application/json" -d '{"search":[{"identifier":"randomIdentifier1","DateOut":"2018-03-18","Destination":"FAO","Origin":"BGY"},{"identifier":"randomIdentifier2","DateOut":"2018-04-21","Destination":"STN","Origin":"VNO"}]}' http://[localhost]/api/ryanair/flights
```

Klaidos įrašomos į storage/logs/errors.log

Testai saugomi tests/GretaiTest.php

# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
