<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airlines\AirlineFactory;

class FlightController extends Controller
{           
    /**
     * Atvaizduojam oro linijų skrydžius pagal parametrus
     * 
     * @param Request $request
     * @param strin $airline
     */
    public function showFlights(Request $request, string $airline)
    {
        $goodParams = true;
        $params = $request->get('search');
        
        /*
         * Tikrinam ar egzistuoja oro linijos
         */
        try {
            $airline = AirlineFactory::build($airline);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }

        /*
         * Tikrinam ar naudotojas pateikė tinkamus parametrus
         */
        if (!empty($params) && is_array($params)) {
            foreach ($params as $param) {
                if (!is_array($param)) {
                    $goodParams = false;
                } 
            }
        } else {
            $goodParams = false;
        }

        if ($goodParams) {
            $returnFligts = $airline->getFlights($params);

            return response()->json($returnFligts, 200);
        } else {
            $airline->logError(get_class($airline), $request->all());
            
            return response()->json(['message' => 'Bad request'], 404);
        }

        /*
            Pavyzdiniai / testiniai parametrai
    
            $params = [];

            $param = [
                'Identifier' => 'randomIdentifier1',
                'DateOut' => '2018-03-18',
                'Destination' => 'BRU',
                'Origin' => 'VLC'
            ];

            $params[] = $param;

            $param2 = [
                'identifier' => 'randomIdentifier2',
                'DateOut' => '2018-04-21',
                'Destination' => 'STN',
                'Origin' => 'VNO'
            ];

            $params[] = $param2;

            $searchString = http_build_query($params);
            parse_str($searchString, $params);

            $airline = AirlineFactory::build($airline);
            $returnFligts = $airline->getFlights($params);

            return response()->json($returnFligts, 200);

            ***********
            curl -X GET -H "Content-Type: application/json" -d '{"search":[{"identifier":"randomIdentifier1","DateOut":"2018-03-18","Destination":"FAO","Origin":"BGY"},{"identifier":"randomIdentifier2","DateOut":"2018-03-18","Destination":"FAO","Origin":"BGY"}]}' http://greitai/api/ryanair/flights 
        */
    }
}
