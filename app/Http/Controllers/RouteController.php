<?php

namespace App\Http\Controllers;

use App\Airlines\AirlineFactory;

class RouteController extends Controller
{   
    /**
     * Atvaizduojam tiesioginius oro linijų skrydžius
     * 
     * @param string $airline
     */
    public function showRoutes(string $airline)
    {
        try {
            $airline = AirlineFactory::build($airline);

            $directRoutes = $airline->getRoutes('direct');
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }    

        return response()->json($directRoutes, 200);
    }
}
