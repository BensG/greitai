<?php

namespace App\Airlines;

interface HttpClientInterface
{
    public function __construct();
    public function request(string $requestMethod, string $url, array $params = [], array $headers = []);
}