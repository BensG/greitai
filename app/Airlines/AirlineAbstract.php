<?php

namespace App\Airlines;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class AirlineAbstract 
{
    abstract protected function getRoutes(): array;
    abstract protected function getFlights(array $params): array;

    /**
     * Loginam klaidas
     * 
     * @param string $airline
     * @param array $error
     * 
     * @return void
     */
    public function logError(string $airline, array $error): void
    {
        $errorLog = new Logger('error');
        $errorLog->pushHandler(new StreamHandler(storage_path('logs/errors.log')), Logger::ERROR);
        $errorLog->info($airline, $error);
    }
}