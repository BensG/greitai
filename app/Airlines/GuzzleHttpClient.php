<?php

namespace App\Airlines;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

class GuzzleHttpClient implements HttpClientInterface
{
    protected $client = null;

    public function __construct() 
    {
        $this->client = new Client();
    }

    /**
     * Įvykdom užklausą
     * 
     * @param string $requestMethod
     * @param string $url
     * @param array $queryParams
     * @param array $headers
     * 
     * @return array
     */
    public function request(string $requestMethod, string $url, array $queryParams = [], $headers = []): array
    {
        $returnData = [
            'success' => 1,
            'response' => '',
            'errors' => []
        ];
        
        try {
            $response = $this->client->request(
                $requestMethod,
                $url,
                [
                    'query' => $queryParams,
                    'headers' => $headers,
                    'connect_timeout' => 10
                ]
            );

            $returnData['response'] = json_decode($response->getBody());
        } catch (ConnectException $e) {
            $returnData['success'] = 0;
            $returnData['response'] = $e->getMessage();

            $returnData['errors'] = [
                'statusCode' => 408,
                'reasonPhrase' => $e->getMessage()
            ];
        } catch (RequestException $e) {
            $returnData['success'] = 0;
            $returnData['response'] = $e->getMessage();
            
            $returnData['errors'] = [
                'statusCode' => $e->getResponse()->getStatusCode(),
                'reasonPhrase' => $e->getResponse()->getReasonPhrase()
            ]; 
    
        };

        return $returnData;
    }
}