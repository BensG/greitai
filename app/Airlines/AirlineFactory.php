<?php

namespace App\Airlines;

class AirlineFactory
{
    /**
     * Inicializuojam oro linijų objektą
     * 
     * @param string $airline
     * 
     * @return object
     */
    public static function build(string $airline, string $httpClient = 'GuzzleHttpClient')
    {
        $airline = 'App\Airlines\\' . ucfirst($airline);
        $httpClient = 'App\Airlines\\' . ucfirst($httpClient);
        
        if (class_exists($airline) && class_exists($httpClient)) {
            return new $airline(new $httpClient());
        } else {
            throw new \Exception("Invalid airline type given.");
        }
    }
}