<?php

namespace App\Airlines;

use App\Airlines\HttpClientInterface;

class Ryanair extends AirlineAbstract
{
    const DOES_NOT_OPERATE = 10;
    const FLIGHT_SOLD_OUT = 11;
    const FLIGHT_NOT_FOUND = 12;
    const WRONG_REQUEST = 20;
    const OTHER_ERROR = 90;

    protected $client = null;

    protected $requestHeaders = [
        'Accept' => 'application/json, text/plain, */*',
        'Accept-Encoding' => 'gzip, deflate, br',
        'Accept-Language' => 'en-US,en;q=0.9',
        'Connection' => 'keep-alive',
        'Host' => 'desktopapps.ryanair.com',
        'Origin' => 'https://www.ryanair.com',
        'Referer' => 'https://www.ryanair.com/lt/lt/booking/home',
        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36'
    ];
    
    private $routes = [];

    protected $urls = [
        'routes' => 'https://api.ryanair.com/core/3/routes',
        'flights' => 'https://desktopapps.ryanair.com/v4/lt-lt/availability'
    ];

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Gaunam skrydžius
     * 
     * @param string $type
     * 
     * @return array
     */
    public function getRoutes(string $type = 'all'): array
    { 
        $response = $this->client->request('GET', $this->urls['routes'], $this->requestHeaders);
        
        $routes = empty($response['errors']) && is_array($response['response']) 
            ? $this->processRoutes($response['response'], $type)
            : $response;
        
        return $routes;
    }

    /**
     * Filtruojam, formuojam ir grąžinam skrydžius
     * 
     * @param array $routes
     * @param string $type
     * 
     * @return array
     */
    private function processRoutes(array $routes = [], string $type = 'all'): array
    {
        $routes = array_values(array_filter($routes, function ($route) use ($type) {
            if ($type == 'ryanair') {
                return $route->operator === 'RYANAIR';
            } else if ($type == 'direct') {
                return empty($route->connectingAirport) && $route->operator === 'RYANAIR';
            } else {
                return $route;
            }
        }));

        $routes = array_map(function ($route) {
            return ["origin" => $route->airportFrom, "destination" => $route->airportTo];
        }, $routes);

        return $routes;
    }

    /**
     * Darom užklausas į Ryanair, apdoruojam rezultatus ir grąžinam duomenis naudotojaui
     * 
     * @param array $params
     * 
     * @return array
     */
    public function getFlights(array $params): array 
    {
        $returnFligts = [];
        $errors = [];

        $preparedParams = $this->prepareParams($params);

        $this->routes = $this->getRoutes('ryanair');

        if (empty($this->routes['errors'])) {
            foreach ($preparedParams as $param) {
                if ($this->validateParams($param)) {
                    if ($this->isRouteExists($param)) {
                        $response = $this->client->request('GET', $this->urls['flights'], $param, $this->requestHeaders);
                    
                        $flight = $this->processFlight($response, $param['identifier']);
    
                        $returnFligts[$param['identifier']] = $flight[$param['identifier']];
                        
                        if (!empty($flight['error'])) {
                            $errors[] = $flight['error'];
                        }  
                    } else {
                        $returnFligts[$param['identifier']] = null;
                        $errors[] = ['identifier' => $param['identifier'], 'code' => self::DOES_NOT_OPERATE];
                    }
                } else {
                    $returnFligts[$param['identifier']] = null;
                    $errors[] = ['identifier' => $param['identifier'], 'code' => self::WRONG_REQUEST];
                }
            }

            $returnFligts['errors'] = $errors;
        } else {
            $returnFligts = $this->routes['errors'];
        }
        
        return $returnFligts;
    }

    /**
     * Užklausos parametrų papildomą validacija
     * 
     * @param $params
     * 
     * @return boolean
     */
    protected function validateParams($params)
    {
        $flightRequestKeys = ['dateout', 'origin', 'destination'];

        foreach ($flightRequestKeys as $flightRequestKey) {
           if (!array_key_exists($flightRequestKey, $params)) return false;
        }

        $dateTimeFromInput = \DateTime::createFromFormat('Y-m-d', $params['dateout']);
    
        if (
            !$dateTimeFromInput 
            || $dateTimeFromInput->format('Y-m-d') !== $params['dateout'] 
            || $params['dateout'] < date("Y-m-d")
        ) return false;
        
        if (
            strlen($params['origin']) != 3
            || strlen($params['destination']) != 3 
            || $params['origin'] == $params['destination']
        ) return false;

        return true;
    }

    /**
     * Tikrinam ar egzistuoja skrydžio maršrutas
     * 
     * @param $params
     * 
     * @return boolean
     */
    private function isRouteExists($params)
    {
        $routeExists = false;

        $searchRoute = ['origin' => $params['origin'], 'destination' => $params['destination']];

        foreach ($this->routes as $route) {
            if ($route == $searchRoute) {
                $routeExists = true;
                break;
            }
        }

        return $routeExists;
    }

    /**
     * Paruošiam parametrus skrydžiams gauti
     * 
     * @param array $params
     * 
     * @return array
     */
    private function prepareParams(array $params): array
    {
        foreach ($params as $key => $values) {
            $params[$key] = array_change_key_case($params[$key], CASE_LOWER);

            if (!array_key_exists('identifier', $params[$key])) {
                $params[$key]['identifier'] = rand();
            }

            $params[$key]['ADT'] = 1;
            $params[$key]['INF'] = 1;
            $params[$key]['CHD'] = 1;
            $params[$key]['IncludeConnectingFlights'] = 'true';
            $params[$key]['RoundTrip'] = 'false';
            $params[$key]['ToUs'] = 'AGREED';

        }

        return $params;
    }

    /**
     * Apdoruojam skrydžio duomenis, kurie bus grąžinami naudotojui
     * 
     * @param array $response
     * @param string $identifier
     * 
     * @return array
     */
    private function processFlight(array $response, string $identifier): array
    {
        $returnFlight = [];

        $flight = $response['response'];

        if (!$response['success']) {
            $returnFlight[$identifier] = null;
            
            $returnFlight['error']['identifier'] = $identifier;
            if ($response['errors']['statusCode'] == 408) {
                $returnFlight['error']['code'] = self::OTHER_ERROR;
            } else {
                $returnFlight['error']['code'] = self::WRONG_REQUEST;
            }
            
            $this->logError(get_class(), $response);
        } else if (count($flight->trips[0]->dates[0]->flights) == 0) {
            $returnFlight[$identifier] = null;

            $returnFlight['error'] = [
                'identifier' => $identifier,
                'code' => self::FLIGHT_NOT_FOUND
            ];
        } else if (!isset($flight->trips[0]->dates[0]->flights[0]->regularFare)) {
            $returnFlight[$identifier] = null;

            $returnFlight['error'] = [
                'identifier' => $identifier,
                'code' => self::FLIGHT_SOLD_OUT
            ];
        } else {
            $flightShortcut = $flight->trips[0]->dates[0]->flights[0];

            $returnFlight[$identifier] = [
                'origin' => $flight->trips[0]->origin,
                'destination' => $flight->trips[0]->destination,
                'departureTime' => date('Y-m-d H:i:s', strtotime($flightShortcut->time[0])),
                'arrivalTime' => date('Y-m-d H:i:s', strtotime($flightShortcut->time[1])),
                'duration' => $this->convertDurationToMinutes($flightShortcut->duration),
                'price' => $this->getFares($flightShortcut->regularFare->fares, $flight->currency),
                'segments' => $this->getSegments($flightShortcut->segments)
            ];
        }

        return $returnFlight;
    }

    /**
     * Sutvarkom ir grąžinam skrydžio kainas
     * 
     * @param array $fares
     * @param string $currency
     * 
     * @return array
     */
    private function getFares(array $fares, string $currency): array
    {
        $returnFares = [];

        foreach ($fares as $fare) {
            $fare = [
                'paxType' => $fare->type,
                'amount' => round($fare->amount * 1.02, 2),
                'currency' => $currency
            ];

            $returnFares[] = $fare;
        }

        $returnFares[] = [
            'paxType' => 'INF',
            'amount' => 25,
            'currency' => $currency
        ];

        return $returnFares;
    }

    /**
     * Sutvarkom ir grąžinam skrydžius
     * 
     * @param array $segments
     * 
     * @return array
     */
    private function getSegments(array $segments): array
    {
        $returnSegments = [];

        foreach ($segments as $segment) {
            $segment = [
                'origin' => $segment->origin,
                'destination' => $segment->destination,
                'departureTime' => date('Y-m-d H:i:s', strtotime($segment->time[0])),
                'arrivalTime' => date('Y-m-d H:i:s', strtotime($segment->time[1])),
                'flightNumber' => preg_replace('/\s+/', '', $segment->flightNumber),
                'duration' => $this->convertDurationToMinutes($segment->duration)
            ];

            $returnSegments[] = $segment;
        }

        return $returnSegments;
    }

    /**
     * Skrydžio trukmę grąžinam minutėmis
     * Pvz.: 2:15 => 135 min
     * 
     * @param string $duration
     * 
     * @return int
     */
    protected function convertDurationToMinutes(string $duration) : int
    {
        list($hours, $minutes) = explode(':', $duration);

        return $hours * 60 + $minutes;
    }
}